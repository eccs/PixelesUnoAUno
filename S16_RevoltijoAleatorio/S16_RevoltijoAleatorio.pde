size(512,344);
// Cargar la imagen
PImage foto = loadImage("TrajineraSejo.jpg");
// 
println("Total de pixeles: "+foto.width*foto.height);
// Acceder a pixeles
foto.loadPixels();
// Ciclo de pixeles
for(int indice=0; indice<foto.width*foto.height; indice+=1){
  // Lectura
  //color pixel = foto.pixels[0];// Constante
  //color pixel = foto.pixels[(foto.width*foto.height-1)-indice];// Medio espejo
  color pixel = foto.pixels[ int(random(foto.width*foto.height)) ]; // Aleatorio
  // Escritura
  foto.pixels[indice] = pixel;
}
foto.updatePixels();
// mostrar imagen
image(foto,0,0);
// Guardar imagen
save("FotoRevoltijo.jpg");