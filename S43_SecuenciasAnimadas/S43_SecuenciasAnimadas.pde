import processing.video.*;
Capture camara;
PImage[] imagenes;
int indiceGrabacion;
int indicePlay;
int indiceReversa;
int indiceOtro;
boolean grabando;
void setup(){
  size(1024,768);
  camara = new Capture(this, 640, 480);
  camara.start();
  
  imagenes = new PImage[30];
  for(int indice=0; indice<imagenes.length; indice++){
   imagenes[indice] = createImage(640,480,RGB); 
  }
  indiceGrabacion = 0;
  indicePlay = 0;
  indiceReversa = imagenes.length-1;
  indiceOtro = 0;
  grabando = false;
}

void draw(){
  if(camara.available()){
   camara.read(); 
  }
  
  image(camara,0,0,width/2,height/2);
  
  if(grabando){
    println("Grabando imagen "+indiceGrabacion);
    imagenes[indiceGrabacion].set(0,0,camara);
    indiceGrabacion++;
    if(indiceGrabacion>=imagenes.length){
     grabando = false; 
     indiceGrabacion = 0;
    }
  }
  
  image(imagenes[indicePlay],width/2,0,  width/2, height/2);
  image(imagenes[indiceReversa],0,height/2, width/2,height/2);
  image(imagenes[indiceOtro],width/2,height/2, width/2,height/2);
  indicePlay = (indicePlay+1) % imagenes.length;
  indiceReversa = (imagenes.length+indiceReversa-1)%imagenes.length;
 // indiceOtro = (indiceOtro+2) % imagenes.length;
  indiceOtro = int(random(imagenes.length));
}

void keyPressed(){
  if(key=='g'){
   grabando = true; 
  }
  if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
   save("Imagen"+hora+".jpg");
  }
  
}