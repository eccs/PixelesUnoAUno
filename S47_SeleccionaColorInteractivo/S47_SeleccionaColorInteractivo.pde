PImage imagen;
color pixel;
void setup(){
 size(512,344);
 imagen = loadImage("TrajineraSejo.jpg");
}
void draw(){
 image(imagen, 0, 0); 
 int columna = mouseX;
 int fila = mouseY;
 int indice = fila*imagen.width + columna;
 imagen.loadPixels();
 pixel = imagen.pixels[indice];
 imagen.updatePixels();
 fill(pixel);
 stroke(255);
 rect(0,0,100,100);
 rect(mouseX,mouseY, 50,50);
}

void mousePressed(){
  float rojo = red(pixel);
  float verde = green(pixel);
  float azul = blue(pixel);
 println("Color: "+rojo+","+verde+","+azul);
 save("Imagen"+hour()+":"+minute()+":"+second()+".jpg");
}