import processing.video.*;
Capture camara;
PImage crayon;
PImage fotoOriginal;
PImage foto;
PImage resultado;
void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 crayon = createImage(camara.width, camara.height, RGB);
 fotoOriginal = loadImage("TrajineraSejo.jpg");
 foto = createImage(camara.width, camara.height, RGB);
 resultado = createImage(camara.width, camara.height, RGB);
 foto.copy(fotoOriginal, 0,0,fotoOriginal.width, fotoOriginal.height,
                         0,0,foto.width, foto.height);
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
 camara.loadPixels(); crayon.loadPixels();
 foto.loadPixels(); resultado.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   // Entrada de datos
   color pixelCamara = camara.pixels[indice];
   color pixelCrayon = crayon.pixels[indice];
   float brillo = brightness(pixelCamara); // 0 a 255
   float saturacion = saturation(pixelCamara); // 0 a 255
   float brilloCrayon = brightness(pixelCrayon); // 0 a 255
   // Procedimiento de decisión y salida de datos (crayón / máscara)
   crayon.pixels[indice] = color(brilloCrayon-8);
   if( brillo > 230 && saturacion < 50){
     crayon.pixels[indice] = color(255);
   }
   // Segundo proceso
   // Entrada de datos
   color pixelFoto = foto.pixels[indice];
   pixelCrayon = crayon.pixels[indice];
   brilloCrayon = brightness(pixelCrayon); // 0 a 255
   // Procesamiento
   float brilloCrayon0a1 = map( brilloCrayon, 0, 255, 0,1);   // Cambia de rango
   // Multiplica / "pesa" cada componente de color
   float rojoPixel = red(pixelFoto) * brilloCrayon0a1;
   float verdePixel = green(pixelFoto)* brilloCrayon0a1;
   float azulPixel = blue(pixelFoto)* brilloCrayon0a1;
   // Salida de datos   
   resultado.pixels[indice] = color( rojoPixel, verdePixel, azulPixel);
 }
 camara.updatePixels(); crayon.updatePixels();
 foto.updatePixels(); resultado.updatePixels();
 // Dibuja imágenes
 image(camara,0,0, width/2, height/2);
 image(crayon,0,height/2, width/2, height/2);
 image(foto,width/2,0, width/2, height/2);
 image(resultado,width/2,height/2, width/2, height/2);
}
void keyPressed(){
 if(key=='s'){
  String hora = hour()+":"+minute()+":"+second();
  save("Ventana"+hora+".jpg");
  crayon.save("Crayon"+hora+".jpg");
  resultado.save("Resultado"+hora+".jpg");
 }
}