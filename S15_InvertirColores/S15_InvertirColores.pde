// Crear ventana
size(1024,687);
// Cargamos la imagen
PImage foto = loadImage("Foto.jpg");
// 
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice+=1){
 // Lectura
 color pixel = foto.pixels[indice];
 float rojo = red(pixel);
 float verde = green(pixel);
 float azul = blue(pixel);
 // Procedimiento
 color nuevoColor = color(100-rojo, 255-verde, 100-azul);
 // Escritura
 foto.pixels[indice] = nuevoColor;
}
foto.updatePixels();
// Mostrar imagen
image(foto,0,0);
// Guardar imagen
save("FotoInvertida_MundoVerde.jpg");