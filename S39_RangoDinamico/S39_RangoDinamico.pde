import processing.video.*;
Capture camara;
PImage resultado;
PImage foto1, foto2;
void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 resultado = createImage(camara.width, camara.height, RGB);
 foto1 = createImage(camara.width, camara.height, RGB);
 foto2 = createImage(camara.width, camara.height, RGB);
}
void draw(){
 if(camara.available()){
   camara.read();
 }
 camara.loadPixels();
 float maxRojo = -1;
 float minRojo = 256;
 float maxVerde = -1; float minVerde = 256;
 float maxAzul = -1; float minAzul = 256;
 for(int indice=0; indice<camara.width*camara.height; indice++){
   color pixel = camara.pixels[indice];
   float rojo = red(pixel);
   float verde = green(pixel);
   float azul = blue(pixel);
   if ( rojo > maxRojo ){
     maxRojo = rojo; 
   }
   if( rojo < minRojo){
    minRojo = rojo; 
   }
   if( verde > maxVerde ){
    maxVerde = verde; 
   }
   if( verde < minVerde ){
    minVerde = verde; 
   }
   if( azul> maxAzul ){
    maxAzul = azul; 
   }
   if( azul<minAzul ){
    minAzul = azul; 
   }
 }
 
 print("MIN "+minRojo+","+minVerde+", "+minAzul+"   ");
 println("MAX "+maxRojo+", "+maxVerde+", "+maxAzul);
 resultado.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   // Entrada
   color pixel = camara.pixels[indice];
   float rojo = red(pixel);
   float verde = green(pixel);
   float azul = blue(pixel);
   // Ajuste de rango dinámico
   float nuevoRojo = map(rojo, minRojo, maxRojo, 200, 255);
   float nuevoVerde = map(verde, minVerde, maxVerde, 0, 255);
   float nuevoAzul = map(azul, minAzul, maxAzul, 100, 200);
   // Salida
   resultado.pixels[indice] = color(nuevoRojo, nuevoVerde, nuevoAzul);
 }
 resultado.updatePixels();
 camara.updatePixels();
 
 image(camara,0,0,width/2, height/2);
 image(resultado,width/2,0, width/2, height/2);
 image(foto1,0,height/2, width/2, height/2);
 image(foto2,width/2,height/2,width/2,height/2);
}
void keyPressed(){
 if(key=='1'){
   foto1.set(0,0,resultado);
   println("Guardada foto 1");
 }
 else if(key=='2'){
   foto2.set(0,0,resultado);
   println("Guardada foto 2");
 }
 else if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
  save("Ventana"+hora+".jpg");
 }
  
}