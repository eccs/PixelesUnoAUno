// Crear ventana
size(512,344);
// Cargar imagen
PImage foto = loadImage("TrajineraSejo.jpg");
int totalPixeles = width*height;
int pixelesFila = width;
// Mostrar imagen en ventana con ciclo
loadPixels();
foto.loadPixels();
for(int indice=0; indice<totalPixeles; indice++){
  // Obteniendo columna y fila
  int columna = indice % width;
  int fila = indice / width;
  // Calculando nueva columna
  int nuevaColumna = (columna*5 + 200) % width;
  // Calcular nuevo Indice
  int nuevoIndice = fila*width + nuevaColumna;
  // Obtener color de nuevo indice
  color pixelFoto = foto.pixels[nuevoIndice];
  // Asignar color de nuevo indice
  pixels[indice] = pixelFoto;
}
updatePixels();
foto.updatePixels();
save("Recorrido200_5.jpg");