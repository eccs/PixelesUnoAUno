import processing.svg.*; // Importar las herramientas SVG
size(512,344);
beginRecord(SVG, "CirculosVectores_50.svg"); // Empieza a "grabar"
PImage foto = loadImage("TrajineraSejo.jpg");
background(0);
colorMode(HSB, 360, 100, 100);
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice++){
 int fila = indice / width;
 int columna = indice % width;
 if(columna%50 == 0 && fila%50==0){
   color pixel = foto.pixels[indice];
   float brillo = brightness(pixel);
   float ancho = map(brillo, 0, 100, 30, 50);
   stroke(pixel); // Color del punto
   strokeWeight(ancho); // Ancho basado en el brillo
   point(columna, fila); // Punto en x,y
 }
}
foto.updatePixels();
endRecord(); // Termina de grabar
//save("Circulos.jpg");