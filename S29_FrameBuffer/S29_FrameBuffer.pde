import processing.video.*;
Capture camara;
PImage salida;
void setup(){
 //fullScreen();
 size(1024,768);
 camara = new Capture(this,640,480);
 camara.start();
 // Crea una imagen "vacía"
 salida = createImage(camara.width, camara.height, RGB);
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
 // Dibujar la imagen
 camara.loadPixels();
 salida.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
  color pixel = camara.pixels[indice];
  float brillo = brightness(pixel);
  salida.pixels[indice] = color(brillo);
 }
 camara.updatePixels();
 salida.updatePixels();
 // Dibuja la imagen de salida
 image(salida,0,0,width,height);
 
}