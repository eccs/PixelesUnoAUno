// Ejercicios posibles
/*
1) Hacer que funcione en columnas
2) Iniciar el slit scan con una tecla
3) Guardar la imagen cuando se completas
*/
import processing.video.*;
Capture camara;
int numeroDeCaptura;
int filaAPintar;
void setup(){
  size(640,480);
  camara = new Capture(this, 640, 480);
  camara.start();
  numeroDeCaptura = 1;// Inicializamos variable
  filaAPintar = 0;
}
void draw(){
  if( camara.available() ){
    camara.read();
  }
  loadPixels();// Cargamos pixeles ventana
  camara.loadPixels(); // Cargamos pixeles de la imagen de la camara
  for(int indice=0; indice<width*height; indice++){ // Recorremos todos los pixeles
   int fila = indice/width; // Calcula fila para el indice dado
   int columna = indice%width;
   if( fila == filaAPintar ){ // Si la fila es la que quiero pintar
     color pixelCamara = camara.pixels[indice]; // Obtén color del pixel
     pixels[indice] = pixelCamara; // Pinta pixel
   }
  }
  camara.updatePixels();
  updatePixels();
  //filaAPintar++; // Fila a pintar se incrementa en 1
  filaAPintar = ( filaAPintar + 1) % height; // Fila a pintar se incrementa y se mantiene en el rango
}
// Esta función se llama con cualquier tecla presionada
void keyPressed(){ 
  if ( key == 's'){//Si se presiona s
    save("Captura"+numeroDeCaptura+".jpg");
    println("Imagen guardada: "+numeroDeCaptura);
    numeroDeCaptura++;// Incrementa contador de capturas
  }
}