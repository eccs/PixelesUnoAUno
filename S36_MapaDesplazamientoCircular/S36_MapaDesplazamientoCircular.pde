import processing.video.*;
Capture camara;
PImage mapa;
PImage resultado;
void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 mapa = createImage(camara.width,camara.height,RGB);
 resultado = createImage(camara.width, camara.height, RGB);
 mapa.loadPixels();
 for(int indice=0; indice<mapa.width*mapa.height; indice++){
   int fila = indice/mapa.width;
   int columna = indice%mapa.width; 
   //float brillo = map(columna, 0, mapa.width-1, 255, 0);// Gradiente horizontal
   //float brillo = map(fila, 0, mapa.height-1, 0, 255); // Gradiente vertical
   float distancia = dist(columna, fila, mapa.width/2, mapa.height/2);
   float brillo = map(distancia, 0, mapa.width/2,  255, 0); // Gradiente radial
   mapa.pixels[indice] = color(brillo);
 }
 mapa.updatePixels();
}

void draw(){
  if(camara.available() ){
    camara.read();
  }
  camara.loadPixels(); mapa.loadPixels(); resultado.loadPixels();
  for(int indice=0; indice<camara.width*camara.height; indice++){
    // Entradas / Inputs
    int fila = indice/camara.width;
    int columna = indice%camara.width;
    float brillo = brightness(mapa.pixels[indice]);
    // Procedimiento
    int desplazamiento = int( map(brillo, 0, 255, -camara.width, camara.width) );
    int nuevaColumna = ( columna + desplazamiento + camara.width) % camara.width;
   // int nuevaColumna = constrain( columna + desplazamiento, 0, camara.width-1); 
    int nuevoIndice = fila*camara.width + nuevaColumna;
    // Salida
    resultado.pixels[indice] = camara.pixels[nuevoIndice];
  }
  camara.updatePixels(); mapa.updatePixels(); resultado.updatePixels();
  
  // Dibuja imágenes
  image(camara,0,0,          width/2, height/2);
  image(mapa,width/2,0,      width/2, height/2); 
  image(resultado,width/4,height/2, width/2, height/2);
}
void keyPressed(){
 if(key=='s'){
  String hora = hour()+":"+minute()+":"+second();
  save("Ventana"+hora+".jpg");
  resultado.save("Resultado"+hora+".jpg");
  println("Imagen guardada");
 }
}