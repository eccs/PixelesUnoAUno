import processing.video.*;
Capture camara;
int total;
float suma,promedio;
int cuenta0, cuenta255;
void setup(){
 size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();
 total = camara.width*camara.height; // Total de pixeles
}

void draw(){
 if(camara.available()){
  camara.read(); 
 }
 camara.loadPixels();
 suma = 0;
 cuenta0 = 0;
 cuenta255 = 0;
 for(int indice=0; indice<total; indice++){
   color pixel = camara.pixels[indice];
   float brillo = brightness(pixel);
   if( int(brillo) == 0){ // si brillo es 0
     cuenta0++; // Aumenta 1 a la cuenta
   }
   else if(int(brillo) == 255){
    cuenta255++; 
   }
   suma += brillo;
   camara.pixels[indice] = color(brillo);
 }
 promedio = suma/total;
 print("Promedio: "+promedio);
 print(" Total de 0: "+cuenta0);
 println(" Total de 255: "+cuenta255);
 
 camara.updatePixels(); 
 image(camara,0,0,width,height);
 // Dibuja punto de promedio
 stroke(promedio);
 strokeWeight(20);
 point(100,100);
}