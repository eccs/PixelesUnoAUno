// Crear ventana
size(640,430);
// Cargar foto
PImage foto = loadImage("Trajinera.jpg");
// Acceder a pixeles
foto.loadPixels();
// Ciclo para leer y modificar pixeles
for(int indice = 0; indice<foto.width*foto.height; indice += 1){
  // Leer
  color pixel = foto.pixels[indice];
  float rojo = red(pixel);
  float verde = green(pixel);
  float azul = blue(pixel);
  // Escritura de un canal
  foto.pixels[indice] = color(rojo);
  // Promedio
  //foto.pixels[indice] = color((rojo+azul+verde)/3);
}
// Actualizar pixeles
foto.updatePixels();
// Mostrar imagen
image(foto, 0,0);
// Guardar imagen
save("Trajinera_Rojo.jpg");
//exit();