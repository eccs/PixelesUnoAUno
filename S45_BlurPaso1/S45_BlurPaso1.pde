import processing.video.*;
Capture camara;
PImage resultado;

void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 resultado = createImage(camara.width, camara.height, RGB);
}

void draw(){
 if(camara.available()){
  camara.read(); 
 }
 camara.loadPixels(); resultado.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   int columnaActual = indice % camara.width;
   int filaActual = indice / camara.width;
   color pixelActual = camara.pixels[indice]; 
   
   int columnaAnterior = constrain( columnaActual - 1 , 0, camara.width-1);
   int columnaSiguiente = constrain( columnaActual + 1, 0, camara.width-1);
   
   int indiceAnterior = filaActual*camara.width + columnaAnterior;
   int indiceSiguiente = filaActual*camara.width + columnaSiguiente;
   
   color pixelAnterior = camara.pixels[indiceAnterior];
   color pixelSiguiente = camara.pixels[indiceSiguiente];
   
   float promedioRojo = (red(pixelAnterior) + red(pixelActual) + red(pixelSiguiente))/3;
   float promedioVerde = (green(pixelAnterior) + green(pixelActual) + green(pixelSiguiente))/3;
   float promedioAzul = (blue(pixelAnterior) + blue(pixelActual) + blue(pixelSiguiente))/3;
   
   resultado.pixels[indice] = color(promedioRojo, promedioVerde, promedioAzul);
 }
 camara.updatePixels(); resultado.updatePixels();
 
 image(camara,0,0, width/2, height/2);
 image(resultado,width/2,0, width/2, height/2);
}