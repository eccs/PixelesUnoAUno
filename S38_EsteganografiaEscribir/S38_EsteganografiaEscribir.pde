/*
char caracter = 72;
char caracter2 = 111;
println(int(caracter));
println(int(caracter2));
println(caracter);
println(caracter2);
*/
size(512,344);
PImage foto = loadImage("TrajineraSejo.jpg");
String mensaje = "Este es el mensaje";
foto.loadPixels();
for(int indice=0; indice<mensaje.length(); indice++){
 char caracter = mensaje.charAt(indice);
 color pixel = foto.pixels[indice];
 float rojo = red(pixel);
 float verde = green(pixel);
 float azul = blue(pixel);
 foto.pixels[indice] = color(rojo, verde, caracter);
 println(int(caracter));
}
foto.updatePixels();
image(foto,0,0);

foto.save("Resultado.jpg"); // Esta tiene distorsión (compresión)
foto.save("Resultado.png"); // Esta no tiene distorsión (compresión)