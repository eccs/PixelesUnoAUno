import processing.video.*;
Capture camara;
PImage fotoOriginal;
PImage foto;
PImage resultado;
PImage resultado2;
void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 fotoOriginal = loadImage("TrajineraSejo.jpg");
 foto = createImage(camara.width, camara.height, RGB);
 resultado = createImage(camara.width, camara.height, RGB);
 foto.copy(fotoOriginal, 0,0,fotoOriginal.width, fotoOriginal.height,
                         0,0,foto.width, foto.height);
 resultado2 = createImage(camara.width, camara.height,RGB);
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
  camara.loadPixels(); foto.loadPixels(); resultado.loadPixels();
  resultado2.loadPixels();
  for(int indice=0; indice < camara.width*camara.height; indice++){
   // Resultado 1
   color pixelCamara = camara.pixels[indice];
   float brillo = brightness(pixelCamara);
   int fila = indice/camara.width;
   int columna = indice%camara.width;
   // Procedimiento
   // Mapea el brillo a un rango determinado
   int desplazamiento = int(map(brillo, 0, 255, 0, camara.height));
   // Calcula una nueva fila sumando el desplazamiento
   int nuevaFila = (fila + desplazamiento) % camara.height;
   // Calcula el nuevo índice a partir de la nueva fila y la columna actual
   int nuevoIndice = nuevaFila*camara.width + columna;
   // Obtén el pixel de la foto en el nuevo índice
   color pixelFoto = foto.pixels[nuevoIndice];
   // Asigna color al resultado
   resultado.pixels[indice] = pixelFoto;
   
   // Resultado 2
   pixelFoto = foto.pixels[indice];
   brillo = brightness(pixelFoto);
   desplazamiento = int( map(brillo, 0, 255, 0, camara.height) );
   nuevaFila = (fila + desplazamiento ) % camara.height;
   nuevoIndice = nuevaFila*camara.width + columna;
   pixelCamara = camara.pixels[nuevoIndice];
   resultado2.pixels[indice] = pixelCamara;
  }
  camara.updatePixels(); foto.updatePixels(); resultado.updatePixels();
  resultado2.updatePixels();
  
  image(camara,0,0, width/2, height/2);
  image(foto, width/2, 0, width/2, height/2);
  image(resultado, 0, height/2, width/2, height/2);
  image(resultado2, width/2, height/2, width/2, height/2);
}

void keyPressed(){
 if(key=='s'){
  String hora = hour()+":"+minute()+":"+second();
  save("Ventana"+hora+".jpg");
  resultado.save("Resultado"+hora+".jpg");
  resultado2.save("Resultado2-"+hora+".jpg");
  println("Imágenes guardadas");
 }
}