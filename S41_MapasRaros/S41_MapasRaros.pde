import processing.video.*;
Capture camara;
PImage mapaRaro1, mapaRaro2, mapaRaro3;

void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 
 mapaRaro1 = createImage(640, 480, RGB);
 mapaRaro2 = createImage(640, 480, RGB);
 mapaRaro3 = createImage(640, 480, RGB);
 colorMode(HSB);
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
 camara.loadPixels(); mapaRaro1.loadPixels();
 mapaRaro2.loadPixels();mapaRaro3.loadPixels();
 // Resetea mapa raro
 for(int indice=0; indice<mapaRaro1.width*mapaRaro1.height; indice++){
  mapaRaro1.pixels[indice] = color(0); 
  mapaRaro2.pixels[indice] = color(0);
  mapaRaro3.pixels[indice] = color(0);
 }
 // Genera mapa raro
 for(int indice=0; indice<camara.width*camara.height; indice++){
   color pixel = camara.pixels[indice];
   float tono = hue(pixel);
   float saturacion = saturation(pixel);
   float brillo = brightness(pixel);
   
   // S y B como ejes
   float x = map(saturacion, 0, 255, 0, mapaRaro1.width-1);
   float y = map(brillo, 0, 255, mapaRaro1.height-1, 0);
   int nuevaColumna = int(x);
   int nuevaFila = int(y);
   int nuevoIndice = nuevaFila*mapaRaro1.width + nuevaColumna;
  // mapaRaro1.pixels[nuevoIndice] = color(pixel);//Color original
   mapaRaro1.pixels[nuevoIndice] = color(tono, saturacion, 255); // H y S originales
   
   // H y S como ejes
   x = map(saturacion, 0, 255, 0, mapaRaro2.width-1);
   y = map(tono, 0, 255, mapaRaro2.height-1, 0);
   nuevaColumna = int(x);
   nuevaFila = int(y);
   nuevoIndice = nuevaFila*mapaRaro2.width + nuevaColumna;
   mapaRaro2.pixels[nuevoIndice] = color(tono, saturacion, 255);
   
   // H y B
   x = map(brillo, 0, 255, mapaRaro3.width-1,0);
   y = map(tono, 0, 255, mapaRaro3.height-1, 0);
   nuevaColumna = int(x);
   nuevaFila = int(y);
   nuevoIndice = nuevaFila*mapaRaro3.width + nuevaColumna;
   mapaRaro3.pixels[nuevoIndice] = color(tono, saturacion, 255);
   
 }
 mapaRaro2.updatePixels(); mapaRaro3.updatePixels();
 camara.updatePixels(); mapaRaro1.updatePixels();
 image(camara,0,0, width/2, height/2); 
 image(mapaRaro1,width/2,0, width/2, height/2);
 image(mapaRaro3,0,height/2, width/2, height/2);
 image(mapaRaro2,width/2,height/2,width/2, height/2);
}
void keyPressed(){
 if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
   save("Imagen"+hora+".jpg");
   println("Imagen guardada");
 }
}