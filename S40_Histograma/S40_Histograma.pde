import processing.video.*;
Capture camara;
int total;
int[] cuentas;
PImage grafica;
void setup(){
 size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();
 total = camara.width*camara.height; // Total de pixeles
 
 grafica = createImage(255,100,RGB);
 cuentas = new int[256];
}

void draw(){
 if(camara.available()){
  camara.read(); 
 }
 camara.loadPixels();
 /*
 cuentas[0] = 0;
 cuentas[1] = 0;
 cuentas[2] = 0;
 cuentas[3] = 0;
 cuentas[...] = 0;
 cuentas[255] = 0;*/
 for(int indice=0; indice<256; indice++){ // Resetea cuentas
   cuentas[indice] = 0;
 }
 // Recorre imagen y cuenta
 for(int indice=0; indice<total; indice++){
   color pixel = camara.pixels[indice];
   float brillo = brightness(pixel);
   // Forma ineficiente:
   /*for(int indiceCuenta=0; indiceCuenta< 256; indiceCuenta++){
     if( int(brillo) == indiceCuenta){ // si brillo es "0"
       cuentas[indiceCuenta]++; // Aumenta 1 a la cuenta
     }
   }*/
   // Forma eficiente
   int indiceCuenta = int(brillo);
   cuentas[indiceCuenta]++;
   camara.pixels[indice] = color(brillo);
 }
 print(" Total de 0: "+cuentas[0]);
 println(" Total de 255: "+cuentas[255]);
 camara.updatePixels();
 grafica.loadPixels();
 for(int indice=0; indice<grafica.width*grafica.height; indice++){
   int columna = indice%grafica.width;
   int frecuencia = cuentas[columna]; // Obtén cuenta correspondiente a columna
   float frecuenciaMapeada = map(frecuencia,0, total/100, 0,255); // Mapea a brillo
   grafica.pixels[indice] = color(frecuenciaMapeada); // Asigna brillo
 }
 grafica.updatePixels();
 image(camara,0,0,width,height);
 image(grafica,0,0,width,100);
}
void keyPressed(){
  if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
   save("Resultado"+hora+".jpg");
   println("Imagen guardada");
  }
}