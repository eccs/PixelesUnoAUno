  // Cambia columnas de la siguiente forma
  // 0 <> width -1    == Ultima columna        == 511
  // 1 <> width -1 -1 == Penúltima columna     == 510
  // 2 <> width -1 -2 == Antepenúltima columna == 509
  // ... 
  // 509 <> 2  == width -1 -509
  // 510 <> 1  == width -1 -510
  // 511 <> 0  == width -1 -511
PImage foto;
// Setup
size(512,344);
foto = loadImage("TrajineraSejo.jpg");

// Draw
loadPixels();
foto.loadPixels();
for(int indice=0; indice<width*height; indice++){
  int fila = indice/width;
  int columna = indice%width;
  int nuevaFila = fila; // Misma fila
  //int nuevaFila = height-1-fila; // Invierte fila
  //int nuevaColumna = columna; // Misma columna
  int nuevaColumna = width-1 - columna; // Invierte columna
  int nuevoIndice = nuevaFila*width + nuevaColumna;
  pixels[indice] = foto.pixels[nuevoIndice];
}
updatePixels();
foto.updatePixels();
//save("ImagenInvertidaH.jpg");