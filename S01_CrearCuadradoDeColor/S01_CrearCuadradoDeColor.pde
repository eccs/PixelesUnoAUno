// Este programa crea una imagen
// Cuadrada de cierto color
// Por: Sejo Vega-Cebrián 2017

// Crea una ventana de 400x400
size(400, 400);
// Asigna el color al fondo
background(230,80,200);
// Guarda imagen
save("MiCuadrado.png");
// Termina el programa
exit();