size(700,700);
int N  = 49; // Número de imágenes
PImage[] imagenes = new PImage[N];
PImage[] cuadrados = new PImage[N];
int numFilas = 7; // Número de filas
int numColumnas = numFilas;
int numCasillas = numFilas*numColumnas;
int anchoCasilla = width/numColumnas;
int altoCasilla = height/numFilas;

for(int indice=0; indice<N; indice++){
 imagenes[indice] = loadImage((indice+1)+".jpg"); 
 cuadrados[indice] = createImage(anchoCasilla,altoCasilla,RGB);

  PImage imagen = imagenes[indice];
  PImage cuadrado = cuadrados[indice];
  
  int anchoFuente = imagen.height;
  int altoFuente = imagen.height;
  int xfuente = (imagen.width - imagen.height)/2;
  int yfuente = 0;
  cuadrado.copy(imagen,  xfuente,yfuente, anchoFuente, altoFuente,
                         0,0,  cuadrado.width, cuadrado.height);
}

for(int indice=0; indice<numCasillas; indice++){
  int fila = indice/numColumnas;
  int columna = indice%numColumnas;
  int x = columna * anchoCasilla;
  int y = fila * altoCasilla;
  int indiceCuadrado = indice % N;
  image(cuadrados[indiceCuadrado],x,y,anchoCasilla,altoCasilla);
  noFill();
  stroke(255);
  strokeWeight(3);
  rect(x,y,anchoCasilla,altoCasilla);
}

save("49.jpg");