import processing.video.*;
Capture camara;
PImage imagen1;
PImage imagen2;
void setup(){
 size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();
 imagen1 = createImage(camara.width, camara.height, RGB);
 imagen2 = createImage(camara.width,camara.height,RGB);
}

void draw(){
  background(0);
  if( camara.available() ){
   camara.read(); 
  }
  if( frameCount%90 == 0){ // Captura imagen cada 90f
   imagen1.set(0,0,camara); // Copiar los pixeles de camara en imagen1
   println("Número de frame: "+frameCount);
  }
  camara.loadPixels();
  imagen1.loadPixels();
  imagen2.loadPixels();
  for(int indice=0; indice<camara.width*camara.height; indice++){
    // Entrada de datos
    color pixelCamara = camara.pixels[indice];
    color pixelImagen1 = imagen1.pixels[indice];
    // Procesamiento
    float nuevoRojo = ( red(pixelCamara) + red(pixelImagen1) )/2;
    float nuevoVerde = ( green(pixelCamara) + green(pixelImagen1))/2;
    float nuevoAzul = ( blue(pixelCamara) + blue(pixelImagen1) )/2 ;
    // Salida de datos
    imagen2.pixels[indice] = color(nuevoRojo, nuevoVerde, nuevoAzul);
  }
  camara.updatePixels();
  imagen1.updatePixels();
  imagen2.updatePixels();
  
  image(camara,0,0,          width/2,height/2);
  image(imagen1,0,height/2,   width/2, height/2);
  image(imagen2,width/2,height/4, width/2,height/2);
}

void keyPressed(){
  if(key == 's'){
   save("Imagen"+minute()+":"+second()+".jpg");
   imagen2.save("FrameBuffer"+minute()+":"+second()+".jpg");
   println("Imagen guardada");
  }
}