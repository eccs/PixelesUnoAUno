size(640,430);
// Modo de color: Hue de 0 a 360, Sat de 0 a 100, Bri de 0 a 100
colorMode(HSB,360,100,100);
// Cargar imagen
PImage foto = loadImage("Trajinera.jpg");
// Acceso a pixeles
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice+=1){
 // Lectura de pixel
 color pixel = foto.pixels[indice];
 float tono = hue(pixel);
 float sat = saturation(pixel);
 float brillo = brightness(pixel);
 // Escritura del pixel
 foto.pixels[indice] = color(brillo*3.6, sat, tono*100/360);
}
foto.updatePixels();
// Mostrar foto
image(foto,0,0);
//
save("BrilloComoTonoYTonoComoBrillo.jpg");
