size(600,600);
// Ambas fotos son del mismo tamaño
// 600x600
PImage foto1 = loadImage("Rosas.jpg");
PImage foto2 = loadImage("TrajineraCortada.jpg");
foto1.loadPixels();
foto2.loadPixels();
for(int indice=0; indice<foto1.width*foto1.height; indice+=1){
  // Lectura de pixeles (INPUT)
  color pixel1 = foto1.pixels[indice];
  color pixel2 = foto2.pixels[indice];
  // Procedimiento: 
  // Pesos aplicados a los componentes de color con suma (o resta, multiplicación...)
  float rojo = red(pixel1)*0.9 + red(pixel2)*0.1;
  float verde = green(pixel1)*1.3 + green(pixel2)*0.7;
  float azul = blue(pixel1)*0.0 + blue(pixel2)*1.0;
  // Escritura del pixel (OUTPUT)
  foto1.pixels[indice] = color(rojo, verde, azul);
}
foto1.updatePixels();
foto2.updatePixels();
image(foto1,0,0);
save("MezclaLoca4.jpg");