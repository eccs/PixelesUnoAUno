// Crear ventana
size(427,220);
// Cargar imagen
PImage foto = loadImage("foto.jpg");
// Muestra numero de pixeles
println("Total de pixeles: "+(foto.width*foto.height));
// Solicitar acceso a pixeles
foto.loadPixels();
// Modificar pixel a verde
int indice = 10000; // Indice inicial
while(indice <= 40000){ // Repite "mientras" se cumple esta condición
  foto.pixels[indice] = color(0,255,0); // Realiza una operación con el índice dado
  indice = indice + 1; // Incrementa cierta cantidad el índice
}
// Cerrar acceso a pixeles
foto.updatePixels();
// Dibujar imagen
image(foto, 0,0);
// Guardar imagen
save("fotoModificada.jpg");