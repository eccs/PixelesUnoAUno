import processing.video.*;
Capture camara;
PImage resultado;
boolean estaGrabando;
void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 resultado = createImage(camara.width, camara.height, RGB);
 estaGrabando = false;
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
  camara.loadPixels(); resultado.loadPixels();
  for(int indice=0; indice<camara.width*camara.height; indice++){
    int fila = indice/camara.width;
    int columna = indice%camara.width;
    int nuevaColumna = columna;
    //if ( columna < camara.width/2 ){
    if ( fila < camara.height /2 ){
      nuevaColumna = camara.width - 1 - columna; // Columna opuesta
    }
    int nuevoIndice = fila*camara.width + nuevaColumna; // Nuevo índice
    resultado.pixels[indice] = camara.pixels[nuevoIndice];
  }
  camara.updatePixels(); resultado.updatePixels();
  image(resultado,0,0,width,height);
  
  if(estaGrabando){
    //if(frameCount%2 == 0){ // Graba cada 2
    saveFrame("frames1024/####.tif");
    //}// Cierra
  }
}

void keyPressed(){
 if ( key == 'g'){ // Grabar
   estaGrabando = true;
   println("Empezando a grabar");
 }
 if (key == 'd'){ // Detener
   estaGrabando = false;
   println("Grabación detenida");
 }
}