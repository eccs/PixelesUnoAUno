// Cargar, redimensionar y guardar
// una imagen

// Crear una ventana al 60% de tamaño
size(384, 258);
// Cargar una imagen
PImage foto = loadImage("Trajinera.jpg");
// Mostrar la imagen
image(foto, 0,0, width, height);
// Guardar la imagen
save("TrajineraChica.jpg");
// Salir (Stop automático)
exit();