import processing.video.*;
Capture camara;
PImage imagen1, imagen2, imagen3;
void setup(){
// fullScreen();
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 imagen1 = createImage(camara.width, camara.height, RGB);
 imagen2 = createImage(camara.width, camara.height, RGB);
 imagen3 = createImage(camara.width, camara.height, RGB);
}

void draw(){
 if(camara.available()){
  camara.read(); 
 }
 camara.loadPixels();
 imagen1.loadPixels();
 imagen2.loadPixels();
 imagen3.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   color pixel = camara.pixels[indice]; 
   float rojo = red(pixel);
   float verde = green(pixel);
   float azul = blue(pixel);
   imagen1.pixels[indice] = color(rojo, verde, 0);
   imagen2.pixels[indice] = color(0,verde,azul);
   imagen3.pixels[indice] = color(rojo,0,azul);
 }
 imagen1.updatePixels();
 imagen2.updatePixels();
 imagen3.updatePixels();
 camara.updatePixels();
 // Mostrar imágenes
 image(camara, 0,0,         width/2, height/2);
 image(imagen1, width/2,0,  width/2, height/2);
 image(imagen2, 0, height/2, width/2, height/2);
 image(imagen3, width/2, height/2, width/2, height/2);
}

void keyPressed(){
 if(key=='s'){
  save("Imagen"+year()+"-"+month()+"-"+day()+" "+hour()+":"+minute()+":"+second()+".jpg");
  println("Imagen guardada");
 }
}