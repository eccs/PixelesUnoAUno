// Crear ventana
size(512,344);
// Cargar imagen
PImage foto = loadImage("TrajineraSejo.jpg");
int totalPixeles = width*height;
int pixelesFila = width;
// Mostrar imagen en ventana con ciclo
loadPixels();
foto.loadPixels();
for(int indice=0; indice<totalPixeles; indice++){
  // Obteniendo columna y fila
  int columna = indice % width;
  int fila = indice / width;
  // Calculando nueva columna de cada canal de color
  int nuevaColumnaR = (columna*3 + 100) % width;
  int nuevoIndiceR = fila*width + nuevaColumnaR;
  float rojo = red(foto.pixels[nuevoIndiceR]);
  
  int nuevaColumnaV = (columna*2 + 80) % width;
  int nuevoIndiceV = fila*width + nuevaColumnaV;
  float verde = green(foto.pixels[nuevoIndiceV]);
  
  int nuevaColumnaA = (columna + 0) % width;
  int nuevoIndiceA = fila*width + nuevaColumnaA;
  float azul = blue(foto.pixels[nuevoIndiceA]);
  
  // Asignar color de nuevo indice
  pixels[indice] = color(rojo,verde,azul);
}
updatePixels();
foto.updatePixels();
save("RecorridoRGB100_321.jpg");