// Sketch que accede a la webcam y asigna pixeles a la ventana
/* Ejercicios:
 1) Cambiar de color con tecla(s) y variables
 2) Hacer que lo pasado se desvanezca poco a poco
 3) Ajustar los umbrales con teclas y variables
 4) Hacer que funcione con tonos de color
*/
import processing.video.*;
Capture camara;
int numeroCaptura;
void setup(){
 size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();// Inicia captura de imagen
 numeroCaptura = 1;// Numero inicial de captura guardada
 colorMode(HSB,360,100,100);// Modo de color
 background(120,0,0);// Fondo inicial
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
 loadPixels();// Carga pixeles de la ventana
 camara.loadPixels();  // Carga pixeles de la imagen
 for(int indice=0; indice<width*height; indice++){
  color pixel = camara.pixels[indice]; // Lee pixel de cámara
  float brillo = brightness(pixel);
  float sat = saturation(pixel);
  if(brillo>90 && sat<20){ // Si el test es positivo
   pixels[indice] = color(60,100,100); // Asigna un color
  }
  else{ // Si no
  //  pixels[indice] = color(120,100,50); // Asigna otro color
  }
  //pixels[indice] = pixel; // Asigna pixel a pixel de ventana
 }
 updatePixels();
 camara.updatePixels();
}
void keyPressed(){
 if( key == 'r'){ // Reiniciar fondo
  background(0,0,0); 
 }
 if( key == 's') { // Guardar
  save("ImagenAmarillaNegro90-20-"+numeroCaptura+".jpg");
  println("Se guardó imagen "+numeroCaptura);
  numeroCaptura++;
 }
}