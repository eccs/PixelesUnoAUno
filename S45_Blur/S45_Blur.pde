import processing.video.*;
Capture camara;
PImage resultadoH;
PImage resultadoV;
PImage resultadoTotal;
int N;
void setup(){
 size(1024,768);
 camara = new Capture(this, 640, 480);
 camara.start();
 resultadoH = createImage(camara.width, camara.height, RGB);
 resultadoV = createImage(camara.width, camara.height, RGB);
 resultadoTotal = createImage(camara.width, camara.height, RGB);
 N = 10;
}

void draw(){
 if(camara.available()){
  camara.read(); 
 }
 // Procedimiento Horizontal
 camara.loadPixels(); resultadoH.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   int columnaActual = indice % camara.width;
   int filaActual = indice / camara.width;
   
   float sumaRojo = 0;
   float sumaVerde = 0;
   float sumaAzul = 0;
   int pixelesEnRango = 0;
   for(int desfase = -N; desfase <= N; desfase++){
     int columnaNueva = constrain(columnaActual + desfase, 0, camara.width-1);
     int indiceNuevo = filaActual*camara.width + columnaNueva;
     color pixelNuevo = camara.pixels[indiceNuevo];
     
     sumaRojo += red(pixelNuevo);
     sumaVerde += green(pixelNuevo);
     sumaAzul += blue(pixelNuevo);
     pixelesEnRango++;
   }
   
   float promedioRojo = sumaRojo/pixelesEnRango;
   float promedioVerde = sumaVerde/pixelesEnRango;
   float promedioAzul = sumaAzul/pixelesEnRango;
   
   resultadoH.pixels[indice] = color(promedioRojo, promedioVerde, promedioAzul);
 }
 camara.updatePixels(); resultadoH.updatePixels();
 // Procedimiento Vertical
 camara.loadPixels(); resultadoV.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   int columnaActual = indice % camara.width;
   int filaActual = indice / camara.width;
   
   float sumaRojo = 0;
   float sumaVerde = 0;
   float sumaAzul = 0;
   int pixelesEnRango = 0;
   for(int desfase = -N; desfase <= N; desfase++){
     int filaNueva = constrain(filaActual + desfase, 0, camara.height-1);
     int indiceNuevo = filaNueva*camara.width + columnaActual;
     color pixelNuevo = camara.pixels[indiceNuevo];
     
     sumaRojo += red(pixelNuevo);
     sumaVerde += green(pixelNuevo);
     sumaAzul += blue(pixelNuevo);
     pixelesEnRango++;
   }
   
   float promedioRojo = sumaRojo/pixelesEnRango;
   float promedioVerde = sumaVerde/pixelesEnRango;
   float promedioAzul = sumaAzul/pixelesEnRango;
   
   resultadoV.pixels[indice] = color(promedioRojo, promedioVerde, promedioAzul);
 }
 camara.updatePixels(); resultadoV.updatePixels();
 
 // Procedimiento Vertical después del Horizontal
 resultadoH.loadPixels(); resultadoTotal.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   int columnaActual = indice % camara.width;
   int filaActual = indice / camara.width;
   
   float sumaRojo = 0;
   float sumaVerde = 0;
   float sumaAzul = 0;
   int pixelesEnRango = 0;
   for(int desfase = -N; desfase <= N; desfase++){
     int filaNueva = constrain(filaActual + desfase, 0, camara.height-1);
     int indiceNuevo = filaNueva*camara.width + columnaActual;
     color pixelNuevo = resultadoH.pixels[indiceNuevo];
     
     sumaRojo += red(pixelNuevo);
     sumaVerde += green(pixelNuevo);
     sumaAzul += blue(pixelNuevo);
     pixelesEnRango++;
   }
   
   float promedioRojo = sumaRojo/pixelesEnRango;
   float promedioVerde = sumaVerde/pixelesEnRango;
   float promedioAzul = sumaAzul/pixelesEnRango;
   
   resultadoTotal.pixels[indice] = color(promedioRojo, promedioVerde, promedioAzul);
 }
 resultadoH.updatePixels(); resultadoTotal.updatePixels();
 
 
 image(camara,0,0, width/2, height/2);
 image(resultadoH,width/2,0, width/2, height/2);
 image(resultadoV,0,height/2, width/2,height/2);
 image(resultadoTotal, width/2, height/2, width/2, height/2);
}
void keyPressed(){
 if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
   save("Ventana"+hora+".jpg");
   resultadoTotal.save("Resultado"+hora+".jpg");
 }
 else if(key =='j'){
  if(N>1){
   N--;
   println("N: "+N);
  }
 }
 else if(key == 'k'){
   N++;
   println("N: "+N);
 }
}