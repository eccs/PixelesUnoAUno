size(451,599);
// Modo de color
colorMode(HSB,360,100,100);

PImage foto = loadImage("vela.jpg");
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice+=1){
  color pixel = foto.pixels[indice];
  float brillo = brightness(pixel);
  float saturacion = saturation(pixel);
  float nuevobrillo;
  if( brillo > 99 && saturacion < 20){
    nuevobrillo = 100;
  }
  else {
    nuevobrillo = 0; 
  }
  foto.pixels[indice] = color(0,0,nuevobrillo);
}
foto.updatePixels();
// Muestra foto
image(foto,0,0);
save("BrilloMayorA99SatMenorA20.jpg");