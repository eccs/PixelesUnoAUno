// 1) Recapitular cómo acceder a la cámara web
// 2) Ver cómo capturar imágenes
import processing.video.*;
Capture camara;
int numeroDeCaptura;
void setup(){
  size(640,480);
  camara = new Capture(this, 640, 480);
  camara.start();
  numeroDeCaptura = 1;// Inicializamos variable
}
void draw(){
  if( camara.available() ){
    camara.read();
  }
  image(camara, 0,0);
}
// Esta función se llama con cualquier tecla presionada
void keyPressed(){ 
  if ( key == 's'){//Si se presiona s
    save("Captura"+numeroDeCaptura+".jpg");
    println("Imagen guardada: "+numeroDeCaptura);
    numeroDeCaptura++;// Incrementa contador de capturas
  }
}