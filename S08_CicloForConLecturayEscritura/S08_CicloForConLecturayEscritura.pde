// Crear ventana
size(417,219);
// Cargar imagen en variable
PImage foto = loadImage("foto.jpg");
// Acceder pixeles
foto.loadPixels();
// Crear ciclo para visitar pixeles
for(int indice = 0; indice<=foto.width*foto.height-1; indice += 1 ){
  // Lectura del pixel
  color pixel = foto.pixels[indice];
  float rojo = red(pixel);
  float verde = green(pixel);
  float azul = blue(pixel);
  // Escritura del pixel
  foto.pixels[indice] = color(rojo,120,0);
}
// Actualizar pixeles
foto.updatePixels();
// Mostrar imagen
image(foto,0,0);
// Guardar imagen
save("FotoCool.jpg");