# PixelesUnoAUno
Sketches de las sesiones de Pixeles: uno a uno para aprender a programar manipulando imágenes.

Los videos se encuentran en [open.tube/video-channels/pixeles_uno_a_uno/videos](https://open.tube/video-channels/pixeles_uno_a_uno/videos)

## Trajinera
La foto de la trajinera usada en los ejemplos es de [ProtoplasmaKid](https://commons.wikimedia.org/wiki/User:ProtoplasmaKid), obtenida de [Wikimedia Commons](https://commons.wikimedia.org/wiki/Main_Page) en la siguiente dirección https://commons.wikimedia.org/wiki/File:Trajinera_en_canal_Nativitas.jpg CC-BY-SA 4.0
