import gab.opencv.*;
import java.awt.Rectangle;
import processing.video.*;
Capture camara;
PImage resultado;
OpenCV opencv;
int N;
void setup(){
  size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();
 resultado = createImage(camara.width, camara.height, RGB);
 opencv = new OpenCV(this, camara.width, camara.height);
 opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
 N = 20;
}
void draw(){
 if(camara.available()){
  camara.read(); 
  opencv.loadImage(camara);
 }
 camara.loadPixels(); resultado.loadPixels();
 Rectangle[] caras = opencv.detect();
 
 for(int indice=0; indice<camara.width*camara.height; indice++){
   int columna = indice % camara.width;
   int fila = indice / camara.width;
   color pixel;
   boolean dentroDeCara = false;
   for(int indiceCara=0; indiceCara<caras.length; indiceCara++){
    Rectangle cara = caras[indiceCara];
    int xinicio = cara.x;
    int xfinal = cara.x + cara.width;
    int yinicio = cara.y;
    int yfinal = cara.y + cara.height*110/100;
    if(columna >= xinicio && columna <= xfinal && fila >= yinicio && fila <= yfinal){
     dentroDeCara = true; 
    }
   }
   
   if( dentroDeCara ){
     int nuevaColumna = (columna / N) * N; 
     int nuevaFila = ( fila / N ) * N;
     int nuevoIndice = nuevaFila*camara.width + nuevaColumna;
     pixel = camara.pixels[nuevoIndice];
   }
   else{
     pixel = camara.pixels[indice];
   }
   resultado.pixels[indice] = pixel;
 }
 camara.updatePixels(); resultado.updatePixels();
 image(resultado,0,0);
 
 for(int indice=0; indice<caras.length; indice++){
  Rectangle cara = caras[indice];
  noFill();
  stroke(0,255,0);
  strokeWeight(5);
  //rect(cara.x, cara.y, cara.width, cara.height);
 }
  
}

void keyPressed(){
 if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
   save("Imagen"+N+"_"+hora+".jpg");
 }
 else if(key=='j'){
  if(N>1){
   N--;
   println(N);
  }
 }
 else if(key=='k'){
  N++;
  println(N);
 }
}