// Crear ventana
size(417,219);
// Cargar imagen en variable
PImage foto = loadImage("foto.jpg");
// Acceder pixeles
foto.loadPixels();
// Crear ciclo para visitar pixeles
//int indice = 10000;
//while(indice<=30000){
//  foto.pixels[indice] = color(255,0,0);
//  indice += 1;
//}
// Ciclo for
for(int indice = 0; indice<=30000; indice += 1 ){
  foto.pixels[indice] = color(255,0,0);
}
// Actualizar pixeles
foto.updatePixels();
// Mostrar imagen
image(foto,0,0);