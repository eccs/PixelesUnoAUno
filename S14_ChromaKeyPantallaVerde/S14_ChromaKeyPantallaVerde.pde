size(1024,687);
// Las dos imágenes tienen el mismo tamaño
PImage foto1 = loadImage("FotoCropped.jpg");
PImage foto2 = loadImage("Trajinera.jpg");
// Modo de color HSB
colorMode(HSB,360,100,100);
// Procedimiento
foto1.loadPixels();
foto2.loadPixels();
for(int indice=0; indice<foto1.width*foto1.height; indice+=1 ){
  // Lectura de pixel
  color pixel1 = foto1.pixels[indice];
  color pixel2 = foto2.pixels[indice];
  float tono = hue(pixel1);
  float sat = saturation(pixel1);
  float bri = brightness(pixel1);
  color nuevoColor;
  // Proceso
  // Si la condición da positivo (pantalla verde)
  if( (tono>70 && tono<100) && bri>30){
    nuevoColor = pixel2; // Asigna color de la otra imagen
    //nuevoColor = color(0,0,100); // Asigna blanco
  }
  else { // Si da negativo (no pantalla verde)
    nuevoColor = pixel1; // Asigna color original
    //nuevoColor = color(0,0,0); // Asigna negro
  }
  // Escritura
  foto1.pixels[indice] = nuevoColor;
}
foto1.updatePixels();
foto2.updatePixels();

image(foto1,0,0);
save("FotoCompuesta.jpg");