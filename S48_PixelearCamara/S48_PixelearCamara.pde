import processing.video.*;
Capture camara;
PImage resultado;
int N;
void setup(){
  size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();
 resultado = createImage(camara.width, camara.height, RGB);
 N = 5;
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
 camara.loadPixels(); resultado.loadPixels();

 for(int indice=0; indice<camara.width*camara.height; indice++){
   int columna = indice % camara.width;
   int fila = indice / camara.width;
   int nuevaColumna = (columna / N) * N; 
   /* 0,1,2,3,4 -> 0;   0
     5,6,7,8,9 -> 1;    5
     10,11,12,13,14->2; 10
   */
   int nuevaFila = ( fila / N ) * N;
   int nuevoIndice = nuevaFila*camara.width + nuevaColumna;
   color pixel = camara.pixels[nuevoIndice];
   resultado.pixels[indice] = pixel;
 }
 camara.updatePixels(); resultado.updatePixels();
 image(resultado,0,0);
  
}

void keyPressed(){
 if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
   save("Imagen"+N+"_"+hora+".jpg");
 }
 else if(key=='j'){
  if(N>1){
   N--;
   println(N);
  }
 }
 else if(key=='k'){
  N++;
  println(N);
 }
}