import processing.video.*;
Capture camara;
PImage[] imagenes;
int indiceImagen;

void setup(){
 size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();
 
 // Crea arreglo de imágenes
 imagenes = new PImage[8]; // 8 imágenes
 // Inicializamos el arreglo (creando cada imagen individual)
 for(int indice=0; indice<imagenes.length; indice++){
   imagenes[indice] = createImage(camara.width, camara.height, RGB);
 }
 // Inicialización del ciclo
 indiceImagen = 0;
}

void draw(){
 if(camara.available()){
  camara.read(); 
 }
 image(camara,0,0,                        width/3, height/3);
 image(imagenes[0],width/3,0,             width/3, height/3);
 image(imagenes[1],2*width/3,0,           width/3, height/3);
 
 image(imagenes[2],0,height/3,            width/3,height/3);
 image(imagenes[3],width/3,height/3,      width/3,height/3);
 image(imagenes[4],2*width/3,height/3,    width/3,height/3);
 
 image(imagenes[5],0,2*height/3,          width/3,height/3);
 image(imagenes[6],width/3,2*height/3,    width/3,height/3);
 image(imagenes[7],2*width/3,2*height/3,  width/3,height/3);
 
 if(frameCount%30==0){
   imagenes[indiceImagen].set(0,0,camara);
   indiceImagen++;
   if(indiceImagen == imagenes.length){
     indiceImagen = 0;
   }
  }
}

void keyPressed(){
 if (key == 's'){
  String hora = hour()+":"+minute()+":"+second();
  save("Ventana"+hora+".jpg");
 }
}