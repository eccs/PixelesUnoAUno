import gab.opencv.*;
import java.awt.Rectangle;
import processing.video.*;
Capture camara;
OpenCV opencv;
PImage imagenCara;
PImage imagenOjo;
void setup(){
 size(740,480);
 // Imprime lista de cámaras
 String [] opciones = Capture.list();
 printArray(opciones);
 
 camara = new Capture(this, 640, 480);
 camara.start();
 opencv = new OpenCV(this, camara.width, camara.height);
 opencv.loadCascade(OpenCV.CASCADE_FRONTALFACE);
 
 imagenCara = createImage(100,100,RGB);
 imagenOjo = createImage(100,100,RGB);
}
void draw(){
 if(camara.available()){
  camara.read(); 
 }
 opencv.loadImage(camara);
 Rectangle[] caras = opencv.detect();
 println(caras.length);
 image(camara,0,0,camara.width,camara.height);
 for(int indice=0; indice<caras.length; indice++){
   Rectangle cara = caras[indice];
   noFill();
   strokeWeight(5);
   stroke(255,255,0);
   // Copia cara
   imagenCara.copy(camara, cara.x, cara.y, cara.width, cara.height,
                           0,      0,      imagenCara.width, imagenCara.height);
   // Copia ojo
   imagenOjo.copy(camara, cara.x+cara.width/6, cara.y+cara.height/4, cara.width/4, cara.height/4,
                       0, 0, imagenOjo.width, imagenOjo.height);
   // Cara
   rect(cara.x, cara.y, cara.width, cara.height);
   // Ojo derecho
   image(imagenCara,cara.x+cara.width/6, cara.y+cara.height/4, cara.width/4, cara.height/4);
   rect(cara.x+cara.width/6, cara.y+cara.height/4, cara.width/4, cara.height/4);
   // Ojo izquierdo
   image(imagenCara,cara.x+cara.width*5/6-cara.width/4, cara.y+cara.height/4, cara.width/4, cara.height/4);
   rect(cara.x+cara.width*5/6-cara.width/4, cara.y+cara.height/4, cara.width/4, cara.height/4);
 }
 //image(camara,0,0,width,height);
 image(imagenCara, camara.width, 0);
 image(imagenOjo, camara.width, imagenCara.height);
}

void keyPressed(){
 if(key=='s'){
  String hora = hour()+":"+minute()+":"+second();
  save("Imagen"+hora+".jpg");
  println("Imagen guardada");
 }
}