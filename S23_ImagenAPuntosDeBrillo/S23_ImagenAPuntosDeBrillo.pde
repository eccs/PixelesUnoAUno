size(512,344);
background(0);
PImage foto = loadImage("TrajineraSejo.jpg");
colorMode(HSB, 360, 100, 100); // Modo de color HSB
stroke(0,0,100);// Color del punto (blanco)
for(int indice=0; indice<foto.width*foto.height; indice++){
 // Calcula # de fila y columna
 int columna = indice % width;
 int fila = indice / width;
 // Realiza la operación en determinados múltiplos
 if( columna%5 == 0 && fila%5 == 0){
  color pixel = foto.pixels[indice];//Leer color del pixel
  float brillo = brightness(pixel); // Obtener brillo de dicho color
  float ancho = map( brillo, 0, 100,  0, 5);// Mapea o convierte los valores
  strokeWeight(ancho); // Ancho del punto
  point(columna,fila); // Dibuja punto
 }
}
save("PuntosBrillo5_0-5.jpg");