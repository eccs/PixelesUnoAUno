// Pesos iguales
// Crear ventana
size(640,430);
// Cargar la imagen
PImage foto = loadImage("Trajinera.jpg");
// Ciclo de acceso a pixeles
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice+=1){
 // Leer pixel
 color pixel = foto.pixels[indice];
 float rojo = red(pixel);
 float verde = green(pixel);
 float azul = blue(pixel);
 // Escribir pixel
 foto.pixels[indice] = color(rojo*0.5+100,verde*0.3+60,azul*0.1);
}
foto.updatePixels();
// Mostrar imagen
image(foto,0,0);
// Guardar
save("IntentoDeSepia.jpg");