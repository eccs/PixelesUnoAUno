size(512,344);
// Imagen es del mismo tamaño que la ventana
PImage foto = loadImage("TrajineraSejo.jpg");

loadPixels();
foto.loadPixels();
int nuevaColumna = 0;
int nuevaFila = 0;
for(int indice=0; indice<width*height; indice++){
  // Obtener fila y columna
  int fila = indice/width;
  int columna = indice%width;
  // Calcula/genera nueva fila y/o nueva columna
  nuevaColumna = columna;
  if( columna == 0){
     //nuevaFila = int(random(height));// Fila completamente aleatoria sin condición
     //if ( fila < 200 && fila>100){ // Condición fija
     //if(fila < random(100,200)){ // Condición aleatoria
     float proba = random(100);
     if ( proba < 20 ){ // Probabilidad
       //nuevaFila = ( fila + int(random(100))) % height; // Fila aleatoria en función de la fila actual
       nuevaFila = int(random(height)); // Fila completamente aleatoria
     }
     /*
     else if(proba < 30){ // Otra probabilidad
       nuevaFila = 0; // Asignamos los contenidos de la fila 0
     }*/
     else{
       nuevaFila = fila; // Fila se mantiene igual
     }
  }
  // Calcula nuevo indice a partir de nueva fila y nueva columna
  int nuevoIndice = nuevaFila*width + nuevaColumna;
  // Obtener color del pixel con nuevo indice
  color pixel = foto.pixels[nuevoIndice];
  // Asignar el color al pixel de la ventana
  pixels[indice] = pixel;
}
updatePixels();
foto.updatePixels();
save("NuevaFilaAleatoria_DosProbabilidades_1.jpg");