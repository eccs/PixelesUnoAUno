// Carga, muestra y guarda una imagen
// Crea la ventana
size(640,430);
// Fondo de color
background(240,10,220);
// Carga la imagen
PImage foto = loadImage("Trajinera_en_canal_Nativitas.jpg");
// Dibuja la imagen
image(foto, 100,0);
// Guarda el resultado
save("FotoNueva.jpg");
exit();