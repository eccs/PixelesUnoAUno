import processing.video.*;
Capture camara;//Declaramos variable
void setup(){// Se ejecuta una vez al inicio
  size(640,480);
  // Crear acceso a la camara con resolución 640x480
  camara = new Capture(this, 640, 480);
  // Iniciar
  camara.start();
}
void draw(){// Se ejecuta 30 veces por segundo
  background(0);
  // Si hay una nueva frame
  if( camara.available() ){
    camara.read(); // "equivalente" a loadImage - carga imagen
  }
  // Realiza proceso sin importar si hay nuevo frame
  camara.loadPixels();
    colorMode(HSB,360,100,100);// Modo HSB
    for(int indice=0; indice<camara.width*camara.height; indice++){
      color pixel = camara.pixels[indice];
      float brillo = brightness(pixel);
      int fila = indice/camara.width;
      int columna = indice%camara.width;
      if(columna%40 ==0 && fila%40 == 0){
        stroke(pixel);
        strokeWeight(40);
        point(columna,fila);
      }
    }
    camara.updatePixels();
}