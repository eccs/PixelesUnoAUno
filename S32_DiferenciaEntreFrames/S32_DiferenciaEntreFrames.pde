import processing.video.*;
Capture camara;
PImage imagenPrevia;
PImage diferencia;
PImage diferenciaUmbral;
void setup(){
 size(640,480);
 camara = new Capture(this, 640, 480);
 camara.start();
 imagenPrevia = createImage(camara.width, camara.height, RGB);
 diferencia = createImage(camara.width, camara.height, RGB);
 diferenciaUmbral = createImage(camara.width, camara.height, RGB);
}
void draw(){
  if(camara.available()){
    camara.read();
  }
  camara.loadPixels(); imagenPrevia.loadPixels(); diferencia.loadPixels();
  diferenciaUmbral.loadPixels();
  for(int indice=0; indice<camara.width*camara.height; indice++){
    // Entrada de datos (input)
    color pixelActual = camara.pixels[indice];
    color pixelPrevio = imagenPrevia.pixels[indice];
    // Procesamiento
    float difRojo = abs( red(pixelActual) - red(pixelPrevio) );
    float difVerde = abs( green(pixelActual) - green(pixelPrevio) );
    float difAzul = abs( blue(pixelActual) - blue(pixelPrevio) );
    float difPromedio = (difRojo + difVerde + difAzul)/3;
    // Salida de datos (output)
    diferencia.pixels[indice] = color(difRojo, difVerde, difAzul);
    if( difPromedio > 28 ){
     //diferenciaUmbral.pixels[indice] = color(255,255,0); 
     diferenciaUmbral.pixels[indice] = pixelActual; 
    }
    else{
    // diferenciaUmbral.pixels[indice] = color(20,80,20); 
    }
    
  }
  camara.updatePixels(); imagenPrevia.updatePixels(); diferencia.updatePixels();
  diferenciaUmbral.updatePixels();
  // Dibuja imágenes
  image(camara,0,0,               width/2, height/2);
  image(imagenPrevia,width/2,0,   width/2,height/2);
  image(diferencia,0, height/2,   width/2,height/2);
  image(diferenciaUmbral,width/2,height/2, width/2, height/2);
  
  // Guarda imagen "anterior"
  imagenPrevia.set(0,0, camara);
}

void keyPressed(){
 if(key=='s'){
   String hora = hour()+":"+minute()+":"+second();
   save("Ventana"+hora+".jpg");
   diferencia.save("Diferencia"+hora+".jpg");
   diferenciaUmbral.save("DiferenciaUmbral"+hora+".jpg");
   println("Imágenes guardadas");
 }
 if(key=='c'){
   diferenciaUmbral = createImage(camara.width,camara.height,RGB);
 }
  
}