// Crear la ventana
size(512,344);
// Cargar imagen
PImage foto = loadImage("TrajineraSejo.jpg");
// Ciclo para acceder pixeles de ventana e imagen
loadPixels(); // Pixeles ventana
foto.loadPixels(); // Pixeles foto
int desplazamiento = 0;
for(int indice=0; indice<width*height; indice++){
  // Calcular fila y columna
  int fila = indice/width;
  int columna = indice%width;
  // Calcular nueva columna
  //int nuevaColumna = width/2;// Nueva columna constante
  //int nuevaColumna = (columna + int(random(100))) % width; // Nueva columna aleatoria
  if(columna == 0){
   desplazamiento = int(random(50)); // Desplazamiento aleatorio
   //desplazamiento = 5*fila; // Desplazamiento en función de la fila
  }
  int nuevaColumna = (columna + desplazamiento) % width;
  // Calcular nuevo indice a partir de la nueva columna
  int nuevoIndice = fila*width + nuevaColumna;
  // Obtener color del pixel correspondiente a nuevoIndice
  color pixel = foto.pixels[nuevoIndice];
  pixels[indice] = pixel;
}
updatePixels();
foto.updatePixels();

save("DesplazamientoAleatorioPorFila50.jpg");