size(600,600);
// Modo de color
colorMode(HSB,360,100,100);

PImage foto = loadImage("rosas.jpg");
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice+=1){
  color pixel = foto.pixels[indice];
  float tono = hue(pixel);
  float brillo = brightness(pixel);
  float saturacion = saturation(pixel);
  
  float nuevaSaturacion;
  if( (tono > 340 || tono<20) && saturacion>70 ){
    nuevaSaturacion = saturacion;
  }
  else {
    nuevaSaturacion = 0; 
  }
  foto.pixels[indice] = color(tono,nuevaSaturacion,brillo);
}
foto.updatePixels();
// Muestra foto
image(foto,0,0);
save("RosasDramaticas.jpg");