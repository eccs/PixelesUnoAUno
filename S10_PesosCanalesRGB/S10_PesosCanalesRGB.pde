// Pesos iguales
// Crear ventana
size(640,430);
// Cargar la imagen
PImage foto = loadImage("Trajinera.jpg");
// Ciclo de acceso a pixeles
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice+=1){
 // Leer pixel
 color pixel = foto.pixels[indice];
 float rojo = red(pixel);
 float verde = green(pixel);
 float azul = blue(pixel);
 // Escribir pixel
 foto.pixels[indice] = color(rojo*6.0-200,verde*6.0-200,azul*6.0-200);
}
foto.updatePixels();
// Mostrar imagen
image(foto,0,0);
// Guardar
save("Posterizacion.jpg");