import processing.video.*;
Capture camara;
PImage salida; // Salida (output)
PImage fondo; // Fondo guardado
int numCaptura; 
int vista;
void setup(){
 //fullScreen();
 size(640,480);
 camara = new Capture(this,640,480);
 camara.start();
 // Crea una imagen "vacía" para la salida y el fondo
 salida = createImage(camara.width, camara.height, RGB);
 fondo = createImage(camara.width, camara.height, RGB);
 numCaptura = 1;
 vista = 1;
 background(0);
}
void draw(){
  // Obtener nueva imagen
 if(camara.available()){
  camara.read(); 
 }
 // Dibujar la imagen
 camara.loadPixels();
 fondo.loadPixels();
 salida.loadPixels();
 for(int indice=0; indice<camara.width*camara.height; indice++){
   // Obtener pixeles de camara y fondo
  color pixelCamara = camara.pixels[indice];
  color pixelFondo = fondo.pixels[indice];
  // Calcular diferencias de canales de color
  float diferenciaRojo = abs(red(pixelCamara) - red(pixelFondo));
  float diferenciaVerde = abs(green(pixelCamara) - green(pixelFondo));
  float diferenciaAzul = abs(blue(pixelCamara) - blue(pixelFondo));
  float diferenciaPromedio = (diferenciaRojo+diferenciaVerde+diferenciaAzul)/3;
  // Asignar color a salida
  //salida.pixels[indice] = color(diferenciaPromedio);
  // Revisa diferencia
  if(diferenciaPromedio > 30){
    //salida.pixels[indice] = color(255);// Blanco
    salida.pixels[indice] = pixelCamara;// Pixel original
  }
  else{
   //salida.pixels[indice] = color(0); // Negro
  }
 }
 camara.updatePixels();
 fondo.updatePixels();
 salida.updatePixels();
 
 if(vista == 1 ){ // Dibuja las imágenes de salida, fondo y cámara
   image(camara,  0,0,         width/2,height/2);
   image(fondo,   width/2,0,   width/2,height/2);
   image(salida,  0,height/2,  width/2,height/2);
 }
 else if(vista == 2){ // Dibuja solo la salida
   image(salida,0,0,width,height);
 }
 
}
void keyPressed(){
 if(key == 'f'){ // Captura fondo
   // Asigna al fondo los pixeles de la camara
   fondo.set(0,0,camara);
   println("Fondo guardado");
 }
 else if(key == 's'){
  save("ImagenResultado"+numCaptura+".jpg");
  println("Se guardó imagen "+numCaptura);
  numCaptura++;
 }
 else if(key == 'd'){ // Debug
   vista = 1;
   println("Cambia a vista 1");
 }
 else if(key == 'v'){ // Vista final
  vista = 2;
  println("Cambia a vista 2");
 }
 else if(key == 'b'){ // Background
   salida = createImage(camara.width, camara.height, RGB);
 }
}