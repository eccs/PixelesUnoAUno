// Obtiene info de una imagen y Modifica pixeles
// Crea la ventana
size(407,219);
// Cargar imagen
PImage foto = loadImage("foto.jpg");
// Muestra info
println("Ancho de la foto: "+foto.width);
println("Alto de la foto: "+foto.height);
println("Total de pixeles: "+ (foto.width*foto.height));
// Proceso
foto.loadPixels();
foto.pixels[40000] = color(0, 255, 0);
foto.pixels[40001] = color(0, 255, 0);
foto.pixels[40002] = color(0, 255, 0);
foto.pixels[40003] = color(0, 255, 0);
foto.updatePixels();
// Muestra foto
image(foto, 0,0);
// Guardar foto
save("FotoModificada.jpg");