import processing.video.*;
Capture camara;
int ncolumnas, nfilas, nceldas;
int alto, ancho;
PImage[] imagenes;
int nimagenes;
int indiceGrabar,indicePlay,indicePlayCelda;
void setup(){
 //size(640,480); 
 fullScreen();
 camara = new Capture(this, 640, 480);
 camara.start();
 ncolumnas = 5;
 
 imagenes = new PImage[300];
 nimagenes = imagenes.length;
 for(int indice=0; indice<nimagenes; indice++){
  imagenes[indice] = createImage(camara.width,camara.height,RGB); 
 }
 indiceGrabar = 0;
}

void draw(){
  if(camara.available()){
   camara.read(); 
  }
  // indicePlay apunta a la imagen recién grabada
  indicePlay = (nimagenes + indiceGrabar -1)%nimagenes;
  
 nfilas = ncolumnas;
 nceldas = ncolumnas*nfilas;
 ancho = width/ncolumnas;
 alto = height/nfilas;
  for(int indice=0; indice<nceldas; indice++){
    int columna = indice%ncolumnas;
    int fila = indice/ncolumnas;
    int desfase = 4*indice;
    indicePlayCelda = (10*nimagenes + indicePlay - desfase) % nimagenes;
    image(imagenes[indicePlayCelda],ancho*columna, alto*fila, ancho, alto);
  } 
  
  imagenes[indiceGrabar].set(0,0,camara);
  indiceGrabar = (indiceGrabar + 1) % nimagenes;
}

void keyPressed(){
 if(key=='s'){
  String hora=hour()+":"+minute()+":"+second();
  save("Ventana"+hora+".jpg");
  println("Imagen guardada");
 }
 else if(key=='j'){
  ncolumnas--; 
  println(ncolumnas);
 }
 else if(key=='k'){
  ncolumnas++; 
  println(ncolumnas);
 }
  
}