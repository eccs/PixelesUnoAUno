import processing.video.*;
Capture camara;
PImage fondo;
PImage diferencia;
PImage mascara;
PImage resultado;
PImage fotoOriginal;
PImage foto;

void setup(){
 size(640,480);
 camara = new Capture(this, 640,480);
 camara.start();
 fondo = createImage(camara.width,camara.height,RGB);
 foto = createImage(camara.width, camara.height,RGB);
 diferencia = createImage(camara.width, camara.height, RGB);
 mascara = createImage(camara.width, camara.height, RGB);
 resultado = createImage(camara.width, camara.height, RGB);
 fotoOriginal = loadImage("TrajineraSejo.jpg");
 foto.copy(fotoOriginal, 0,0, fotoOriginal.width, fotoOriginal.height,
                         0,0, foto.width, foto.height);
                         
}
void draw(){
  if ( camara.available() ){
   camara.read(); 
  }
  camara.loadPixels(); fondo.loadPixels(); diferencia.loadPixels();
  mascara.loadPixels(); resultado.loadPixels();
  for(int indice=0 ; indice < camara.width*camara.height; indice++){
    // Input / Entrada de datos
    color pixelCam = camara.pixels[indice];
    color pixelFondo = fondo.pixels[indice];
    // Procesamiento
    float difRojo = abs( red(pixelCam) - red(pixelFondo) );
    float difVerde = abs( green(pixelCam) - green(pixelFondo) );
    float difAzul = abs( blue(pixelCam) - blue(pixelFondo) );
    float maxDif = max( difRojo, difVerde, difAzul );
    // Output / Salida de datos
    diferencia.pixels[indice] = color(difRojo, difVerde, difAzul);
    if(maxDif > 80){ // Si se pasa el umbral
      mascara.pixels[indice] = color(255); // Pixel de máscara blanco
      resultado.pixels[indice] = foto.pixels[indice]; // Pixel de la foto
    }
    else{
     mascara.pixels[indice] = color(0); 
     resultado.pixels[indice] = color(0);
    }
  }
  mascara.updatePixels(); resultado.updatePixels();
  camara.updatePixels(); fondo.updatePixels(); diferencia.updatePixels();
  // Dibuja imágenes
  image(camara,0,0,          width/2,height/2);
  image(fondo,width/2,0,     width/2,height/2);  
  image(diferencia,0,height/2, width/2,height/2);
  image(mascara, width/2, height/2, width/2, height/2);
  image(resultado, width/4, height/4, width/2, height/2);
 // image(foto,0,0);
}

void keyPressed(){
 if(key == 'f' ){
  fondo.set(0,0, camara);
  println("Fondo guardado");
 }
 if(key == 's'){
  String hora = hour()+":"+minute()+":"+second();
  save("Ventana"+hora+".jpg");
  diferencia.save("Diferencia"+hora+".jpg");
  mascara.save("Mascara"+hora+".jpg");
  resultado.save("Resultado"+hora+".jpg");
 }
}