size(512,344);

PImage foto = loadImage("TrajineraSejo.jpg");
int totalPixeles = foto.width*foto.height;

foto.loadPixels();
loadPixels();
for(int indice=0; indice<totalPixeles; indice+=1){
  int nuevoIndice = int(indice*10.7)+totalPixeles/2;//Sumar desfase, multiplicar
  //nuevoIndice = constrain(nuevoIndice,0,totalPixeles-1); // Límite forzado
  nuevoIndice = nuevoIndice % totalPixeles; // Modulo
  color pixel = foto.pixels[nuevoIndice];
  pixels[indice] = pixel;
}
foto.updatePixels();
updatePixels();

//save("RecorrePor10_7.jpg");