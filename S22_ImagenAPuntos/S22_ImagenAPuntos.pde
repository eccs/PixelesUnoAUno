size(512,344);
background(0);
strokeWeight(25);
PImage foto = loadImage("TrajineraSejo.jpg");
foto.loadPixels();
for(int indice=0; indice<foto.width*foto.height; indice++){
  int fila = indice/width;
  int columna = indice%width;
  color pixel = foto.pixels[indice];
  if ( columna%30 == 0 && fila%30==0){ // Si el # de columna es múltiplo de 10
    stroke(pixel);
    point(columna,fila);
  }
}
foto.updatePixels();
save("Cada30columnas30filasFondoNegroSW25.jpg");