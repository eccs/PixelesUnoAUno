// Leer info de un pixel
size(200,200);// Crea ventana
// Carga una foto
PImage foto = loadImage("foto.jpg");
// Escribe info de foto
println("Número total de pixeles: "+(foto.width*foto.height));
// Acceder a pixeles
foto.loadPixels();
color pixel = foto.pixels[30000];
float rojo = red(pixel);
float verde = green(pixel);
float azul = blue(pixel);
foto.updatePixels();
// Muestra info de pixel
println("Rojo: "+rojo+" Verde: "+verde+" Azul: "+azul);
background(rojo, verde, azul);
save("UnPixel.jpg");