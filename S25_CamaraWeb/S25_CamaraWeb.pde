import processing.video.*;
Capture camara;//Declaramos variable
void setup(){// Se ejecuta una vez al inicio
  size(640,480);
  // Crear acceso a la camara con resolución 640x480
  camara = new Capture(this, 640, 480);
  // Iniciar
  camara.start();
}
void draw(){// Se ejecuta 30 veces por segundo
  // Si hay una nueva frame
  if( camara.available() ){
    camara.read(); // "equivalente" a loadImage - carga imagen
    camara.loadPixels();
    colorMode(HSB,360,100,100);// Modo HSB
    for(int indice=0; indice<camara.width*camara.height; indice++){
      color pixel = camara.pixels[indice];
      float brillo = brightness(pixel);
      color nuevoColor = color(120, 100, brillo);// Cambia color
      camara.pixels[indice] = nuevoColor;
    }
    camara.updatePixels();
  }
  // Dibuja la imagen correspondiente o frame
  image( camara, 0,0);
}